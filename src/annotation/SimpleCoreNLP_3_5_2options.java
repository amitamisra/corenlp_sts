package annotation;
import com.beust.jcommander.Parameter;

public class SimpleCoreNLP_3_5_2options {

	@Parameter(
			names={"--posmodel", "-m"},
			required=false,
			description="pos tagger models. Please download from http://nlp.stanford.edu/software/stanford-postagger-2015-12-09.zip  After downloading, uznip the file in lib folder and add to your classpath"
		)
	String posmodel="lib/stanford-postagger-2015-12-09/models/english-bidirectional-distsim.tagger";
		
	 /**
     * @return the posmodel
     */
    public String getposmodel() {
        return posmodel ;
    }	
}

    