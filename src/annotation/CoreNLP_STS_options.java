package annotation;
import java.io.File;

import com.beust.jcommander.Parameter;

public class CoreNLP_STS_options {
		@Parameter(
				names={"--posmodel", "-m"},
				required=false,
				description="pos tagger models. Please download from http://nlp.stanford.edu/software/stanford-postagger-2015-12-09.zip  After downloading, uznip the file in lib folder and add to your classpath"
			)
		String posmodel="lib/stanford-postagger-2015-12-09/models/english-bidirectional-distsim.tagger";
		
		@Parameter(
	            names={"--data-column1"},
	            description="The name of the column where the first text to be evaluated is located",
	            required=true
				 )
		protected String dataColumn1;

	    @Parameter(
	            names={"--data-column2"},
	            description="The name of the column where the second text to be evaluated is located",
	            required=true
	    		)
	    protected String dataColumn2;
	    
	    @Parameter(
	            names={"--input-file"},
	            required=true,
	            description="The input file for feature extraction"
	    		)
	    protected String inputFile;
	    
	    @Parameter(
	            names={"--output-file-name"},
	            required=true,
	            description="The filename for the output file. " 
	    		)
	    protected String ouputFile ;
		 /**
	     * @return the posmodel
	     */
	    public String getposmodel() {
	        return posmodel ;
	    }	
	    /**
	     * @return the dataColumn1
	     */
	    public String getDataColumn1() {
	        return dataColumn1;
	    }

	    /**
	     * @return the dataColumn2
	     */
	    public String getDataColumn2() {
	        return dataColumn2;
	    }
	    
	    /**
	     * @return the inputFile
	     */
	    public String getInputFile() {
	        return inputFile;
	    }

	    /**
	     * @return the outputFile
	     */
	    public String getOutputFile() {
	        return ouputFile;
	    }
	}

	    
